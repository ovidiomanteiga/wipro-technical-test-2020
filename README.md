# Wipro Technical Test - Weather Forecast app

- Author: Ovidio Manteiga Moar.
- Date: 2020-02-25.
- LinkedIn: [`ovidiomanteiga`](https://www.linkedin.com/in/ovidiomanteiga/).

## Build and run instructions

1. Clone this repository.
1. Open the Xcode project file with Xcode 11.
1. The target `WeatherForecast` and a device should be selected (developed with "iPhone 11 Pro Max - iOS 13.3" iOS Simulator).
1. Run the selected target on the selected device.

### Run the `Debug` configuration

By default, the `Release` configuration is selected in the build scheme, but the `Debug` configuration can be set, in which case
[sample data from the Open Weather API](https://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b6907d289e10d714a6e88b30761fae22) is used.

1. Edit the `WeatherForecast` build scheme.
1. Go to the `Run` section.
1. Switch the `Build Configuration` to `Debug`.
1. Close the dialog and run the selected configuration.


## Next steps...

1. Unit test all clases. The design was made so that every class can be tested by mocking their external dependencies.
1. Create UI tests for the main view controller.
1. Refine and document the domain types.
1. Refine the UI design.
1. Implement error management.
1. Improve the icon loading mechanism by making it asynchronous (for example, using the `SDWebImage` library).
1. Improve the dependency injection (for example, using a container like Swinject).
1. Localization and internationalization.
