
import Foundation



struct WeatherForecastDTO: Codable {
    let cod: String?
    let city: CityDTO?
    let list: [WeatherItemDTO]?
}


struct CityDTO: Codable {
    let coord: CoordinatesDTO?
    let country: String?
    let id: Int
    let name: String?
}


struct CoordinatesDTO: Codable {
    let lat: Double?
    let lon: Double?
}


struct WeatherItemDTO: Codable {
    let clouds: WeatherItemCloudsDTO?
    let dt: Int?
    let dt_txt: String?
    let main: WeatherItemMainDTO?
    let rain: WeatherItemRainDTO?
    let snow: WeatherItemSnowDTO?
    let weather: [WeatherItemContidionDTO]?
    let wind: WeatherItemWindDTO?
}


struct WeatherItemMainDTO: Codable {
    let feels_like: Double?
    let grnd_level: Double?
    let humidity: Double?
    let pressure: Double?
    let sea_level: Double?
    let temp: Double?
    let temp_kf: Double?
    let temp_max: Double?
    let temp_min: Double?
}


struct WeatherItemContidionDTO: Codable {
    let description: String?
    let id: Int
    let icon: String?
    let main: String?
}


struct WeatherItemCloudsDTO: Codable {
    let all: Double?
}


struct WeatherItemWindDTO: Codable {
    let deg: Double?
    let speed: Double?
}


struct WeatherItemRainDTO: Codable {
    let _3h: Double?
    enum CodingKeys: String, CodingKey {
        case _3h = "3h"
    }
}


struct WeatherItemSnowDTO: Codable {
    let _3h: Double?
    enum CodingKeys: String, CodingKey {
        case _3h = "3h"
    }
}
