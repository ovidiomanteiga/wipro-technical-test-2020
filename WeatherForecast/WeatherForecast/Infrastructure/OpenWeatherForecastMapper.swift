
import Foundation



class OpenWeatherForecastMapper {
    
    // MARK: - Internal Methods
    
    func map(_ dto: WeatherForecastDTO?) -> WeatherForecast? {
        guard let dto = dto else { return nil }
        return WeatherForecast(hours: dto.list?.compactMap(self.map),
                               location: self.map(dto.city))
    }
    

    func map(_ dto: CityDTO?) -> City? {
        guard let dto = dto else { return nil }
        return City(countryCode: dto.country, id: String(dto.id), name: dto.name)
    }

    
    func map(_ dto: WeatherItemDTO?) -> HourlyWeatherForecast? {
        guard let dto = dto else { return nil }
        let date = self.formatter.date(from: (dto.dt_txt ?? "") + "Z")
        return HourlyWeatherForecast(conditions: dto.weather?.compactMap(self.map),
                                     date: date,
                                     maximumTemperature: dto.main?.temp_max?.asTemperature_kelvin,
                                     minimumTemperature: dto.main?.temp_min?.asTemperature_kelvin,
                                     pressure: dto.main?.pressure?.asPressure_hectoPascals,
                                     rainForecast: self.map(dto.rain),
                                     temperature: dto.main?.temp?.asTemperature_kelvin,
                                     windForecast: self.map(dto.wind))
    }

    
    func map(_ dto: WeatherItemRainDTO?) -> RainForecast? {
        guard let dto = dto else { return nil }
        return RainForecast(volume: dto._3h?.asVolume_millimetersPerSquareMillimeter)
    }
    
    
    func map(_ dto: WeatherItemWindDTO?) -> WindForecast? {
        guard let dto = dto else { return nil }
        return WindForecast(direction: dto.deg?.asAngle_degrees,
                            speed: dto.speed?.asSpeed_metersPerSecond)
    }

    
    func map(_ dto: WeatherItemContidionDTO?) -> WeatherCondition? {
        guard let dto = dto else { return nil }
        let iconURL = dto.icon == nil ? nil : URL(string: "http://openweathermap.org/img/wn/\(dto.icon!).png")
        return WeatherCondition(description: dto.description, iconURL: iconURL, name: dto.main)
    }

    
    // MARK: - Private Properties

    private lazy var formatter: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withSpaceBetweenDateAndTime, .withDashSeparatorInDate,
                                   .withFullDate, .withFullTime, .withColonSeparatorInTime]
        formatter.timeZone = TimeZone(identifier: "UTC")
        return formatter
    }()
    
}


// MARK: - Internal Extensions

extension Double {
    
    var asAngle_degrees: Measurement<UnitAngle> {
        Measurement(value: self, unit: UnitAngle.degrees)
    }

    var asPressure_hectoPascals: Measurement<UnitPressure> {
        Measurement(value: self, unit: UnitPressure.hectopascals)
    }

    var asSpeed_metersPerSecond: Measurement<UnitSpeed> {
        Measurement(value: self, unit: UnitSpeed.metersPerSecond)
    }

    var asTemperature_kelvin: Measurement<UnitTemperature> {
        Measurement(value: self, unit: UnitTemperature.kelvin)
    }

    var asVolume_millimetersPerSquareMillimeter: Measurement<UnitLength> {
        Measurement(value: self, unit: UnitLength.millimeters)
    }

}
