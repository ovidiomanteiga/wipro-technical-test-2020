
import Foundation



class OpenWeatherForecastService: WeatherForecastService {
    
    // MARK: - Initialization
    
    init(client: OpenWeatherAPIClient) {
        self.client = client
    }

    
    // MARK: - WeatherForecastService Implementation

    func get(for location: WeatherForecastLocation) throws -> WeatherForecast? {
        do {
            let result: WeatherForecastDTO? = try self.client.perform(locationID: location.id)
            return self.mapper.map(result)
        } catch _ {
            throw DomainError()
        }
    }
    
    
    // MARK: - Private Properties

    private let client: OpenWeatherAPIClient
    
    private lazy var mapper = OpenWeatherForecastMapper()
    
}
