
import Foundation


// MARK: - Internal Protocols

protocol OpenWeatherAPIClient {

    func perform<Res: Codable>(locationID: String?) throws -> Res?

}


// MARK: - Internal Classes

class BaseOpenWeatherAPIClient: OpenWeatherAPIClient {

    // MARK: - Initialization
    
    init(session: URLSession = .shared) {
        self.session = session
    }

    
    // MARK: - OpenWeatherAPIClient Implementation
    
    func perform<Res: Codable>(locationID: String?) throws -> Res? {
        let params = ["id": locationID ?? "", "appid": self.appID ] as Dictionary<String, String>
        let url = self.baseURL!.appendingPathComponent("forecast")
        var urlBuilder = URLComponents(url: url, resolvingAgainstBaseURL: false)
        urlBuilder?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        var request = URLRequest(url: urlBuilder!.url!)
        let group = DispatchGroup()
        group.enter()
        var result: Res?
        var errorResult: Error?
        let task = self.session.dataTask(with: request) { data, response, error in
            defer { group.leave() }
            if let error = error {
                errorResult = error
                return
            }
            debugPrint(response ?? "NO RESPONSE")
            do {
                result = try JSONDecoder().decode(Res.self, from: data!)
            } catch let e {
                errorResult = e
            }
        }
        task.resume()
        group.wait()
        if let errorResult = errorResult {
            throw errorResult
        }
        return result
    }
    

    // MARK: - Private Properties
    
    private let session: URLSession
    
    fileprivate var appID: String { "" }
    fileprivate var baseURL: URL? { nil }

}


// MARK: - Internal Classes

class DefaultOpenWeatherAPIClient: BaseOpenWeatherAPIClient {
    
    override fileprivate var appID: String { "37c6a0550200f512c8acbe181a95123e" }

    override var baseURL: URL? {
        URL(string: "http://api.openweathermap.org/data/2.5")!
    }
    
}



class SampleOpenWeatherAPIClient: BaseOpenWeatherAPIClient {
    
    override fileprivate var appID: String { "b6907d289e10d714a6e88b30761fae22" }
    
    override fileprivate var baseURL: URL? {
        URL(string: "https://samples.openweathermap.org/data/2.5")!
    }

    
    override func perform<Res: Codable>(locationID: String?) throws -> Res? {
        try super.perform(locationID: Cities.moscow.id)
    }

}
