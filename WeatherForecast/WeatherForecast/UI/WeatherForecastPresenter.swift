
import Foundation



// MARK: - Internal Protocols

protocol WeatherForecastPresenter {

    var delegate: WeatherForecastPresenterDelegate? { get set }
    var interactor: GetWeatherForecastInteractor? { get set }
    var forecast: WeatherForecastViewModel? { get }

    func loadForecast(city: City)
  
}


protocol WeatherForecastPresenterDelegate: class {
    
    func onForecastLoaded()

}


// MARK: - Internal Classes

class DefaultWeatherForecastPresenter: WeatherForecastPresenter {
    
    // MARK: - Initialization
    
    init(delegate: WeatherForecastPresenterDelegate? = nil, interactor: GetWeatherForecastInteractor) {
        self.delegate = delegate
        self.interactor = interactor
    }
    

    // MARK: - WeatherForecastPresenter Implementation - Properties
    
    weak var delegate: WeatherForecastPresenterDelegate?
    
    var forecast: WeatherForecastViewModel?
    var interactor: GetWeatherForecastInteractor?


    // MARK: - WeatherForecastPresenter Implementation - Methods

    func loadForecast(city: City) {
        let forecast = try? self.interactor?.getForecast(for: city)
        self.forecast = WeatherForecastViewModel(forecast: forecast)
        self.delegate?.onForecastLoaded()
    }
    
}
