
import UIKit



class DailyWeatherCell: UITableViewCell {

    // MARK: - Internal Properties
    
    var condition: String? {
        didSet { self.conditionLabel.text = self.condition }
    }
    var conditionIcon: UIImage? {
        didSet { self.conditionImageView.image = self.conditionIcon }
    }
    var date: String? {
        didSet { self.dateLabel.text = self.date }
    }
    var maximumTemperature: String? {
        didSet { self.maximumTemperatureLabel.text = self.maximumTemperature }
    }
    var minimumTemperature: String? {
        didSet { self.minimumTemperatureLabel.text = self.minimumTemperature }
    }

    
    // MARK: - Private Properties

    @IBOutlet
    private weak var dateLabel: UILabel!
    @IBOutlet
    private weak var conditionLabel: UILabel!
    @IBOutlet
    private weak var conditionImageView: UIImageView!
    @IBOutlet
    private weak var maximumTemperatureLabel: UILabel!
    @IBOutlet
    private weak var minimumTemperatureLabel: UILabel!

}
