
import UIKit



class WeatherForecastViewController: UIViewController {

    // MARK: - Injected Properties

    var presenter: WeatherForecastPresenter! {
        didSet { self.presenter.delegate = self }
    }

    
    // MARK: - Lifecycle
    
	override func viewDidLoad() {
		super.viewDidLoad()
        self.cityLabel.text = self.currentCity.name
        self.loadingLabel.text = "Loading \(self.currentCity.name ?? "")'s weather..."
        self.loadingView.isHidden = false
	}
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        OperationQueue().addOperation {
            self.presenter.loadForecast(city: self.currentCity)
        }
    }


    // MARK: - IBOutlets

    @IBOutlet
    private weak var cityLabel: UILabel!
    @IBOutlet
    private weak var conditionImageView: UIImageView!
    @IBOutlet
    private weak var conditionLabel: UILabel!
    @IBOutlet
    private weak var loadingLabel: UILabel!
    @IBOutlet
    private weak var loadingView: UIView!
    @IBOutlet
    private weak var tableView: UITableView!
    @IBOutlet
    private weak var temperatureLabel: UILabel!


    // MARK: - Private Properties

    private let currentCity = Cities.corunna

    fileprivate var forecast: WeatherForecastViewModel? {
        didSet { self.mapForecast() }
    }
    

    // MARK: - Private Methods

    private func mapForecast() {
        self.conditionImageView.image = self.forecast?.conditionIcon
        self.conditionLabel.text = self.forecast?.condition
        self.temperatureLabel.text = self.forecast?.temperature
    }
    
}


// MARK: - UITableViewDataSource Implementation

extension WeatherForecastViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.forecast?.days?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.forecast?.days?[section].hourlyForecast.count ?? 0
    }
    

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        self.forecast?.days?[section].date
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        typealias Cell = DailyWeatherCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "default", for: indexPath) as! Cell
        let hour = self.forecast?.days?[indexPath.section].hourlyForecast[indexPath.row]
        cell.condition = hour?.condition
        cell.conditionIcon = hour?.conditionIcon
        cell.date = hour?.hour
        cell.maximumTemperature = hour?.maximumTemperature
        cell.minimumTemperature = hour?.minimumTemperature
        return cell
    }
    
}


// MARK: - WeatherForecastPresenterDelegate Implementation

extension WeatherForecastViewController: WeatherForecastPresenterDelegate {
    
    func onForecastLoaded() {
        OperationQueue.main.addOperation {
            self.loadingView.isHidden = true
            self.forecast = self.presenter.forecast
            self.cityLabel.text = self.forecast?.cityName
            self.tableView.reloadData()
        }
    }
    
}
