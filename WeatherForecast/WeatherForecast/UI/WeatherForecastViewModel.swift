
import Foundation
import UIKit



class WeatherForecastViewModel {

    // MARK: - Initialization
    
    init(forecast: WeatherForecast?) {
        self.forecast = forecast
        self.cityName = self.forecast?.location?.name
        let firstCondition = self.forecast?.hours?.first?.conditions?.first
        self.condition = firstCondition?.name
        self.conditionIcon = firstCondition?.iconURL?.asImage
        self.temperature = self.format(temperature: self.forecast?.hours?.first?.temperature?.converted(to: .celsius))
        self.map(hours: self.forecast?.hours)
    }
    
    
    // MARK: - Public Properties
    
    private(set) var cityName: String?
    private(set) var condition: String?
    private(set) var conditionIcon: UIImage?
    private(set) var days: [DailyWeatherForecastViewModel]?
    private(set) var temperature: String?
    
    
    // MARK: - Private Properties
    
    private var forecast: WeatherForecast?
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.doesRelativeDateFormatting = true
        return formatter
    }()
    private lazy var hourFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter
    }()
    private lazy var measurementFormatter: MeasurementFormatter = {
        let formatter = MeasurementFormatter()
        formatter.unitOptions = .providedUnit
        formatter.numberFormatter.maximumFractionDigits = 0
        return formatter
    }()

    
    // MARK: - Private Methods

    private func format(temperature: Measurement<UnitTemperature>?) -> String? {
        guard let temperature = temperature else { return nil }
        return self.measurementFormatter.string(from: temperature)
    }

    
    private func map(hours: [HourlyWeatherForecast]?) {
        guard let hours = hours else { return }
        let hourVMs: [HourlyWeatherForecastViewModel] = hours.map {
            let condition = $0.conditions?.first?.name
            var hourText: String? = ""
            if let date = $0.date {
                hourText = self.hourFormatter.string(from: date)
            }
            let icon = $0.conditions?.first?.iconURL?.asImage
            let maxTemp = self.format(temperature: $0.maximumTemperature?.converted(to: .celsius))
            let minTemp = self.format(temperature: $0.minimumTemperature?.converted(to: .celsius))
            return HourlyWeatherForecastViewModel(condition: condition,
                                                 conditionIcon: icon,
                                                 date: $0.date,
                                                 hour: hourText,
                                                 maximumTemperature: maxTemp,
                                                 minimumTemperature: minTemp)
        }
        let groupedHours = Dictionary(grouping: hourVMs) { (hour) -> DateComponents in
            Calendar.current.dateComponents([.day, .year, .month], from: hour.date!)
        }
        let days: [(DateComponents, [HourlyWeatherForecastViewModel])] = groupedHours.sorted { a, b in
            guard let aDate = a.value.first?.date, let bDate = b.value.first?.date
                else { return false }
            return aDate < bDate
        }
        self.days = days.map {
            var dateText = ""
            if let date = $0.1.first?.date {
                dateText = self.dateFormatter.string(from: date)
            }
            return DailyWeatherForecastViewModel(date: dateText, hourlyForecast: $0.1)
        }
    }

}


// MARK: - Internal Types

struct DailyWeatherForecastViewModel {
    
    let date: String?
    var hourlyForecast: [HourlyWeatherForecastViewModel]
    
}


struct HourlyWeatherForecastViewModel {
    
    let condition: String?
    let conditionIcon: UIImage?
    let date: Date?
    let hour: String?
    let maximumTemperature: String?
    let minimumTemperature: String?

}


// MARK: - Internal Extensions

extension URL {
    
    var asImage: UIImage? {
        guard let data = try? Data(contentsOf: self)
            else { return nil }
        return UIImage(data: data)
    }
    
}
