
import UIKit



class SceneDelegate: UIResponder, UIWindowSceneDelegate {

	var window: UIWindow?


	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let forecastController = storyboard.instantiateInitialViewController() as! WeatherForecastViewController
        forecastController.presenter = self.compositor.presenter
        self.window = self.window ?? UIWindow()
        self.window!.rootViewController = forecastController
        self.window!.makeKeyAndVisible()
		guard let _ = (scene as? UIWindowScene) else { return }
	}
    
    
    private let compositor = CompositionRoot()

}

