
import Foundation



class CompositionRoot {
    
    var interactor:  GetWeatherForecastInteractor {
        GetWeatherForecastInteractor(service: service)
    }

    var presenter: WeatherForecastPresenter {
        return DefaultWeatherForecastPresenter(interactor: self.interactor)
    }

    var service: WeatherForecastService {
        OpenWeatherForecastService(client: self.client)
    }
    
    #if DEBUG
    private let client = SampleOpenWeatherAPIClient()
    #else
    private let client = DefaultOpenWeatherAPIClient()
    #endif

}
