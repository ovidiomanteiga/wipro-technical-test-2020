
import Foundation



struct WeatherForecast {
	let hours: [HourlyWeatherForecast]?
	let location: WeatherForecastLocation?
}


struct HourlyWeatherForecast {
    let conditions: [WeatherCondition]?
    let date: Date?
    let maximumTemperature: Measurement<UnitTemperature>?
    let minimumTemperature: Measurement<UnitTemperature>?
    let pressure: Measurement<UnitPressure>?
    let rainForecast: RainForecast?
    let temperature: Measurement<UnitTemperature>?
    let windForecast: WindForecast?
}


struct WeatherCondition {
    let description: String?
    let iconURL: URL?
    let name: String?
}


protocol WeatherForecastLocation {
    var id: String? { get }
    var name: String? { get }
}


struct City: WeatherForecastLocation {
    let countryCode: String?
    let id: String?
    let name: String?
}


struct WindForecast {
	let direction: Measurement<UnitAngle>?
	let speed: Measurement<UnitSpeed>?
}


struct RainForecast {
	let volume: MillimetersPerSquareMillimeter?
}


struct DomainError: Error {}


typealias MillimetersPerSquareMillimeter = Measurement<UnitLength>


struct Cities {
    
    static let corunna = City(countryCode: "ES", id: "3119841", name: "Corunna")
    static let moscow = City(countryCode: "RU", id: "524901", name: "Moscow")
}
