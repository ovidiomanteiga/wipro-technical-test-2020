
import Foundation



class GetWeatherForecastInteractor {

	init(service: WeatherForecastService) {
		self.service = service
	}

	func getForecast(for location: WeatherForecastLocation) throws -> WeatherForecast? {
		try self.service.get(for: location)
	}

	private let service: WeatherForecastService

}



protocol WeatherForecastService {

	func get(for location: WeatherForecastLocation) throws -> WeatherForecast?

}
